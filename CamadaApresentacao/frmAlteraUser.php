<?php 
require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();



require_once('../CamadaControle/Classes/Administrador.php'); 
$objConnection = new Connection();
$objAdministrador = new Administrador();

?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Configurações</h1>
			</header>
		<FORM method="POST">
<?php
			$objAdministrador->visualizarConta();
echo "
			<label>Nome </label><br>
			<input name='nome' type='text' value='" . $objAdministrador->getNome() . "'></input><br>
			<label>Sobrenome </label><br>
			<input name='sobrenome' type='text' value='" . $objAdministrador->getSobrenome() . "'></input><br>
			<label>Senha </label><br>
			<input name='senha' type='text' value='" . $objAdministrador->getSenha() . "'></input><br>
			<label>E-mail </label><br>
			<input name='email' type='text' value='" . $objAdministrador->getEmail() . "'></input><br>
			<label>CPF/CNPJ </label><br>
			<input name='cpfCnpj' type='text' value='" . $objAdministrador->getCpfCnpj() . "'></input><br>				
";	 
?>	
			<button class="btnAlterar" value="salvar" name="salvar">Salvar Alterações</button>
		</form>	
<?php
	if(isset($_POST["salvar"]) && $_POST["salvar"] == "salvar"){
		$nome = $_POST["nome"];
		$sobrenome = $_POST["sobrenome"];
		$senha = $_POST["senha"];
		$cpfCnpj = $_POST["cpfCnpj"];
		$email = $_POST["email"];
		$objAdministrador->setNome($nome);
		$objAdministrador->setSobrenome($sobrenome);
		$objAdministrador->setSenha($senha);
		$objAdministrador->setCpfCnpj($cpfCnpj);
		$objAdministrador->setEmail($email);
		
		$objAdministrador->alterarConta();
	}
?>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>

<?php 
require_once('../CamadaControle/Classes/Login.php');
require_once('../CamadaControle/Classes/Connection.php');

$objLogin = new Login();
$objLogin->verificarLogado();
?>

<!DOCTYPE html>
<html lang='pt-br'>
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
		<title>SGV</title>
		<meta name='description' content='SGMP'>
		<meta name='author' content='Gabriel Dissotti'>
		<meta name='viewport' content='width=device-width; initial-scale=1.0'>
		<link rel='shortcut icon' href='../CamadaApresentacao/midia/user-1.png'>
		<link rel='apple-touch-icon' href='../CamadaApresentacao/midia/user-1.png'>
		<link rel='stylesheet' type='text/css' href='css/style.css' />
		<link rel='stylesheet' type='text/css' href='css/quadroFrm.css' />
		<link href='css/esteps.css' rel='stylesheet' type='text/css' />
	</head>
<?php 



if(isset($_POST['alterar'])){
require_once('../CamadaControle/Classes/Cliente.php');

		$id = $_POST['alterar'];
		$objCliente = new Cliente();		
		$objCliente->visualizarCliente($id);
		
		$nome = $objCliente->getNome();
		$sobrenome = $objCliente->getSobrenome();
		$email = $objCliente->getEmail();
		$telefone = $objCliente->getTelefone();
		$cpfCnpj = $objCliente->getCpfCnpj();
		$cpfCnpj = str_pad($cpfCnpj, 11, "0", STR_PAD_LEFT);
		$rg = $objCliente->getRg();
		$endereco = $objCliente->getEndereco();
		$bairro = $objCliente->getBairro();
		$cidade = $objCliente->getCidade();
		$estado = $objCliente->getEstado();
		$cep = $objCliente->getCep();
		$cep = str_pad($cep, 8, "0", STR_PAD_LEFT);
		$complemento = $objCliente->getComplemento();
		$numero = $objCliente->getNumero();
		}
?>

	<body>
		<div id='view'>
			<header class='quadro'>
				<h1>Alterar Informações de "<?php if(isset($_POST['alterar'])){echo  $nome . ' ' . $sobrenome; }?>" </h1>
				
			</header>
			<form id='cliente_frm' action='' class='frm-medio' method='POST' enctype='multipart/form-data' name='cliente_frm'>
				<input hidden name='id' value='<?php if(isset($_POST['alterar'])){ echo $id;} ?>' />
				<ul id='progress'>
					<li class='ativo'>Informações Pessoais</li>
					<li>Localicação</li>
					<li>Contato</li>
				</ul>
							
				<fieldset>
					<div class='resp'></div>
					<h2>Dados do Cliente</h2>
					<h3>Nome</h3><h3>Sobrenome</h3>
					<input name='nome' type='text' placeholder='Nome' value='<?php if(isset($_POST['alterar'])){echo $nome;} ?>'><br>
					<input name='sobrenome' type='text' placeholder='Sobrenome' value='<?php if(isset($_POST['alterar'])){echo $sobrenome;} ?>'><br>
					<h3>CPF/CNPJ</h3><h3>RG</h3>
					<input name='cpf' id='cpf' type='text' placeholder='CPF/CNPJ' value='<?php if(isset($_POST['alterar'])){echo $cpfCnpj;} ?>'><br>
					<input name='rg' id='rg' type='text' placeholder='RG' value='<?php if(isset($_POST['alterar'])){echo $rg;} ?>'><br>
						
					<input type='button' name='next1' class='next acao' value='Proximo' />
				</fieldset>
				<fieldset class='g_to_left'>
					<div class='resp'></div>
					<h2>Endereço e Localização</h2>
					<h3>Cep</h3>
					<input type='text' min='1' max='24' name='cep' class='maior' placeholder='CEP' id='cep'  value='<?php if(isset($_POST['alterar'])){echo $cep;} ?>'/>
					<h3>Número</h3>
					<input name='numero' class='maior' type='text' placeholder='Número'  value='<?php if(isset($_POST['alterar'])){echo $numero;} ?>' />
					<h3>Complemento</h3>
					<input name='complemento' placeholder='Complemento' class='maior' type='text' value='<?php if(isset($_POST['alterar'])){echo $complemento;} ?>'  />
					<h3>Bairro</h3>
					<input name='bairro' class='maior' type='text' placeholder='Bairro' value='<?php if(isset($_POST['alterar'])){echo $bairro;} ?>' />
					<h3>Cidade</h3>
					<input name='cidade' class='maior' type='text' placeholder='Cidade' value='<?php if(isset($_POST['alterar'])){echo $cidade;} ?>' />
					<h3>Estado</h3>
					<select name='estado' class='maior' id='frm-cad-select'  selected=''>
						<option <?php if(isset($_POST['alterar'])){if('SP' == $estado){echo 'selected';}} ?>>SP</option>
						<option <?php if(isset($_POST['alterar'])){if('AC' == $estado){echo 'selected';}} ?>>AC</option>
						<option <?php if(isset($_POST['alterar'])){if('AL' == $estado){echo 'selected';}} ?>>AL</option>
						<option <?php if(isset($_POST['alterar'])){if('AP' == $estado){echo 'selected';}} ?>>AP</option>
						<option <?php if(isset($_POST['alterar'])){if('AM' == $estado){echo 'selected';}} ?>>AM</option>
						<option <?php if(isset($_POST['alterar'])){if('BA' == $estado){echo 'selected';}} ?>>BA</option>
						<option <?php if(isset($_POST['alterar'])){if('CE' == $estado){echo 'selected';}} ?>>CE</option>
						<option <?php if(isset($_POST['alterar'])){if('DF' == $estado){echo 'selected';}} ?>>DF</option>
						<option <?php if(isset($_POST['alterar'])){if('ES' == $estado){echo 'selected';}} ?>>ES</option>
						<option <?php if(isset($_POST['alterar'])){if('GO' == $estado){echo 'selected';}} ?>>GO</option>
						<option <?php if(isset($_POST['alterar'])){if('MA' == $estado){echo 'selected';}} ?>>MA</option>
						<option <?php if(isset($_POST['alterar'])){if('MT' == $estado){echo 'selected';}} ?>>MT</option>
						<option <?php if(isset($_POST['alterar'])){if('MS' == $estado){echo 'selected';}} ?>>MS</option>
						<option <?php if(isset($_POST['alterar'])){if('MG' == $estado){echo 'selected';}} ?>>MG</option>
						<option <?php if(isset($_POST['alterar'])){if('PA' == $estado){echo 'selected';}} ?>>PA</option>
						<option <?php if(isset($_POST['alterar'])){if('PB' == $estado){echo 'selected';}} ?>>PB</option>
						<option <?php if(isset($_POST['alterar'])){if('PR' == $estado){echo 'selected';}} ?>>PR</option>
						<option <?php if(isset($_POST['alterar'])){if('PE' == $estado){echo 'selected';}} ?>>PE</option>
						<option>PI</option>
						<option>RJ</option>
						<option>RN</option>
						<option>RS</option>
						<option>RO</option>
						<option>RR</option>
						<option>SC</option>
						<option>SE</option>
						<option>TO</option>
						
					</select>
					<h3>Endereco</h3>
					<input name='endereco' style='width:50%;' class='maior'  type='text' placeholder='Endereço' value='<?php if(isset($_POST['alterar'])){echo $endereco;} ?>'/>
					<input type='button' name='prev' class='prev acao' value='Anterior' />
					<input type='button' name='next2' class='next acao' value='Proximo' />
				</fieldset>
				<fieldset>
					<div class='resp'></div>
					<h2>Contato</h2>
					<h3 style='width:18%;margin-left:45px;'>Telefone</h3>
					<input style='width:50%;'  name='telefone' id="telefone" class='maior' type='text' placeholder='Telefone' value='<?php if(isset($_POST['alterar'])){echo $telefone;} ?>' />
					<h3 style='width:18%;margin-left:45px;'> E-mail</h3>
					<input style='width:50%;' name='email' id='email' class='maior' type='text' placeholder='Email' value='<?php if(isset($_POST['alterar'])){echo $email;} ?>' />
					
					<input style='width:37%;' type='button' name='prev' class='prev acao' value='Anterior' />
					<input style='width:37%;' type='submit' name='salvar' class='acao Enviar' value='Atualizar' />
				</fieldset>
			</form>	
		</div>
			<script src='js/animations.js'></script>


<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js'></script>
	
<script src='https://code.jquery.com/jquery-latest.min.js' type='text/javascript'></script>
<script src='js/jquery.maskedinput.min.js' type='text/javascript'></script>
<script>
//mascaras para campos
   $('#cpf').mask('999.999.999-99');
   $('#rg').mask('99.999.999-9');
   $('#cep').mask('99999-999');
   $('#telefone').mask('(99) 9999-9999');
</script>
	
<SCRIPT>
/* FUNÇÃO JQUERY PARA VALIDAR, ENVIAR FORMULARIOS E PASSO A PASSO DOS FORMULARIOS */
$(function(){
	var atual_fs, next_fs, prev_fs;
	var formulario = $('form[name=cliente_frm]');

	function next(elem){
		atual_fs = $(elem).parent();
		next_fs = $(elem).parent().next();


		$('#progress li').eq($('fieldset').index(next_fs)).addClass('ativo');
		atual_fs.hide(300);
		next_fs.show(300);
	}


	$('.prev').click(function(){
		atual_fs = $(this).parent();
		prev_fs = $(this).parent().prev();


		$('#progress li').eq($('fieldset').index(atual_fs)).removeClass('ativo');
		atual_fs.hide(300);
		prev_fs.show(300);
	});
	
	$('input[name=next1]').click(function(){
		var array = formulario.serializeArray();
		if(array[0].value == '' || array[1].value == '' || array[2].value == '' || array[3].value == '' ){
			$('.resp').html("<div class='erros'><p>preencha todos os campos corretamente</p></div>");
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[name=next2]').click(function(){
		var array = formulario.serializeArray();
		if(array[4].value == '' || array[5].value == '' || array[6].value == '' || array[7].value == '' || array[8].value == '' || array[9].value == '' || array[10].value == ''){
			$('.resp').html("<div class='erros'><p>preencha todos os campos corretamente</p></div>");
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
		if(array[11].value == '' || array[12].value == ''){
			$('.resp').html("<div class='erros'><p>preencha todos os campos corretamente</p></div>");
		}else{
			$.ajax({
				type: 'post',
				url: "../CamadaControle/alterarCliente.php",
				data: {salvar: 'sim', campos: array},
				dataType:  'json',
				beforeSend: function(){					
					$('.resp').html("<div class='ok'><p>Validando seus dados... Aguarde um instante</p></div>");
				},
				success: function(valor){
					if(valor.erro == 'sim'){
						$('.resp').html("<div class='erros'><p>" + valor.getErro + "</p></div>");
					}else{
						$('.resp').html("<div class='ok'><p>"+ valor.msg +"</p></div>");
						 window.location.href = 'frmListaCli.php';
					}
				},
				error: function(){
					$('.resp').html("<div class='erros'><p>" + valor.getErro + "</p></div>");
				}
			});
		}
		evento.preventDefault();
	});	
});
// FIM	
	
	
</SCRIPT>
		
				
	</body>
</html>
<!DOCTYPE html>
<?php require_once('../CamadaControle/Classes/Login.php');
	  require_once('../CamadaControle/Classes/Produto.php');
 
$objConnection = new Connection();
$objLogin = new Login();
$objLogin->verificarLogado();
$objProduto = new Produto();

?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Configurações</h1>
			</header>
		<FORM method="POST">
			
		<?php
	if(isset($_POST["+"])){
		$id = $_POST["+"];
		$objProduto->visualizarProduto($id);
			echo"
			<label>Nome </label><br>
			<input name='classe' type='text' value='" . $objProduto->getClasse() . "'></input><br>
			<label>Descricao </label><br>
			<input name='descricao' type='text' value='" . $objProduto->getDescricao() . "'></input><br>
			<label>Valor </label><br>
			<input name='subtotal' type='text' value='" . $objProduto->getSubtotal() . "'></input><br>
			<button class='btnAlterar' value='" . $id . "' name='salvar'>Salvar Alterações</button>
		";
		}
		?>
		</form>	
<?php
	if(isset($_POST["salvar"])){
		$id = $_POST["salvar"];
		$classe 	= $_POST["classe"];
		$descricao 	= $_POST["descricao"];
		$valor 		= $_POST["subtotal"];
		$objProduto->setClasse($classe);
		$objProduto->setDescricao($descricao);
		$objProduto->setSubtotal($valor);
		
		$objProduto->alterarProduto($id);
	}
?>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>

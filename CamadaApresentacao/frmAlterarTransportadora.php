<?php 
if(isset($_POST['alterar'])){
$id = $_POST['alterar'];
require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();
require_once('../CamadaControle/Classes/Connection.php');
require_once('../CamadaControle/Classes/Transportadora.php');
$objTransportadora = new Transportadora();
$objTransportadora->visualizarTransportadora($id);
$nome = $objTransportadora->getNome();
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		<link href="css/esteps.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Alterar dados da Transportadora <?php echo $objTransportadora->getNome(); ?></h1>
			</header>
			<form id="cliente_frm" class="frm-medio" method="POST" enctype="multipart/form-data" name='cliente_frm'>
				<ul id="progress">
				</ul>
				<fieldset>
					<div class="resp"></div>
					<h2>Informações de Transportadora</h2>
					<h3>Nome</h3>
					<input name='id' id="id" hidden value='<?php echo $id ?>'></input>
					<input name='nome' id="nome" type='text' placeholder="Nome" value='<?php  echo $nome;  ?>'>
					<input style="width:37%;" type="submit" name="Enviar" class="acao Enviar" value="Atualizar" />
				</fieldset>
			</form>			
			
		</div>
<?php } ?>
<script src="js/animations.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<SCRIPT>
/* FUNÇÃO JQUERY PARA VALIDAR, ENVIAR FORMULARIOS E PASSO A PASSO DOS FORMULARIOS */
$(function(){
	var formulario = $('form[name=cliente_frm]');
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
		if(array[0].value == ''){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$.ajax({
			type: 'post',
			url: '../CamadaControle/alterarTransportadora.php',
			data: {cadastrar: 'sim', campos: array},
			dataType: 'json',
			beforeSend: function(){
				$('.resp').html("<div class='ok'><p>Validando seus dados... Aguarde um instante</p></div>");
			},
			success: function(valor){
				if(valor.erro == 'sim'){
					$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
				}else{
					$('.resp').html('<div class="ok"><p>'+ valor.msg +'</p></div>');
					 window.location.href = "frmListarTransportadoras.php";
				}
			},
			error: function(){
				$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
			}
			
		});
		}
		evento.preventDefault();
	});		
});
// FIM	
</SCRIPT>
		
				
	</body>
</html>
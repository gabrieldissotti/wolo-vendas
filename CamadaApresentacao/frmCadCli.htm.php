
<?php 
require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();


require_once('../CamadaControle/Classes/Connection.php');
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		<link href="css/esteps.css" rel="stylesheet" type="text/css" />
	</head>
	

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Cadastro de Cliente</h1>
			</header>
			<form id="cliente_frm" class="frm-medio" method="POST" enctype="multipart/form-data" name='cliente_frm'>
				<ul id="progress">
					<li class="ativo">Informações Pessoais</li>
					<li>Localicação</li>
					<li>Contato</li>
				</ul>
							
				<fieldset>
					<div class="resp"></div>
					<h2>Dados do Cliente</h2>
					<h3>Nome</h3><h3>Sobrenome</h3>
					<input name='nome' id='nome' type='text' placeholder="Nome"><br>
					<input name='sobrenome' type='text' placeholder="Sobrenome"><br>
					<h3>CPF/CNPJ</h3><h3>RG</h3>
					<input name='cpf' id="cpf" type='text' placeholder="CPF/CNPJ"><br>
					<input name='rg' id="rg" type='text' placeholder="RG"><br>
						
					<input type="button" name="next1" class="next acao" value="Proximo" />
				</fieldset>
				<fieldset class='g_to_left'>
					<div class="resp"></div>
					<h2>Endereço e Localização</h2>
					<h3>Cep</h3>
					<input type="text" name="cep" class="maior" placeholder="CEP" id="cep" />
					<h3>Número</h3>
					<input name="numero" id="numero" class="maior" type="text" placeholder="Número"  />
					<h3>Complemento</h3>
					<input name="complemento" id='complemento' placeholder="Complemento" class="maior" type="text"  />
					<h3>Bairro</h3>
					<input name="bairro" id="bairro" class="maior" type="text" placeholder="Bairro" />
					<h3>Cidade</h3>
					<input name="cidade" id="cidade" class="maior" type="text" placeholder="Cidade" />
					<h3>Estado</h3>
					<select name='estado' class="maior" id="frm-cad-select">
						<option>SP</option>
						<option>AC</option>
						<option>AL</option>
						<option>AP</option>
						<option>AM</option>
						<option>BA</option>
						<option>CE</option>
						<option>DF</option>
						<option>ES</option>
						<option>GO</option>
						<option>MA</option>
						<option>MT</option>
						<option>MS</option>
						<option>MG</option>
						<option>PA</option>
						<option>PB</option>
						<option>PR</option>
						<option>PE</option>
						<option>PI</option>
						<option>RJ</option>
						<option>RN</option>
						<option>RS</option>
						<option>RO</option>
						<option>RR</option>
						<option>SC</option>
						<option>SE</option>
						<option>TO</option>
						
					</select>
					<h3>Endereco</h3>
					<input name="endereco" id="rua" style="width:50%;" class="maior" type="text" placeholder="Endereço"/>
					<input type="button" name="prev" class="prev acao" value="Anterior" />
					<input type="button" name="next2" class="next acao" value="Proximo" />
				</fieldset>
				<fieldset>
					<div class="resp"></div>
					<h2>Contato</h2>
					<h3 style="width:18%;margin-left:45px;">Telefone</h3>
					<input style="width:50%;"  name="telefone" class="maior"  id="telefone"  type="text" placeholder="Telefone" />
					<h3 style="width:18%;margin-left:45px;"> E-mail</h3>
					<input style="width:50%;" name="email" id="email" class="maior" type="text" placeholder="Email" />
					
					<input style="width:37%;" type="button" name="prev" class="prev acao" value="Anterior" />
					<input style="width:37%;" type="submit" name="Enviar" class="acao Enviar" value="Cadastrar" />
				</fieldset>
			</form>			
			
		</div>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
		<script src="js/animations.js"></script>



<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>


<!-- Adicionando Javascript -->
    <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>



<script>
//mascaras para campos
   $("#cep").mask("99999-999");
   $("#telefone").mask("(99) 9999-9999");
</script>
<SCRIPT>
/* FUNÇÃO JQUERY PARA VALIDAR, ENVIAR FORMULARIOS E PASSO A PASSO DOS FORMULARIOS */
$(function(){
	var atual_fs, next_fs, prev_fs;
	var formulario = $('form[name=cliente_frm]');

	function next(elem){
		atual_fs = $(elem).parent();
		next_fs = $(elem).parent().next();


		$('#progress li').eq($('fieldset').index(next_fs)).addClass('ativo');
		atual_fs.hide(300);
		next_fs.show(300);
	}


	$('.prev').click(function(){
		atual_fs = $(this).parent();
		prev_fs = $(this).parent().prev();


		$('#progress li').eq($('fieldset').index(atual_fs)).removeClass('ativo');
		atual_fs.hide(300);
		prev_fs.show(300);
	});
	
	$('input[name=next1]').click(function(){
		var array = formulario.serializeArray();
		if(array[0].value == '' || array[1].value == '' || array[2].value == '' || array[3].value == '' ){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[name=next2]').click(function(){
		var array = formulario.serializeArray();
		if(array[4].value == '' || array[5].value == '' || array[6].value == '' || array[7].value == '' || array[8].value == '' || array[9].value == '' || array[10].value == ''){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
		if(array[11].value == '' || array[12].value == ''){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$.ajax({
			type: 'post',
			url: '../CamadaControle/cadastrarCliente.php',
			data: {cadastrar: 'sim', campos: array},
			dataType: 'json',
			beforeSend: function(){
				$('.resp').html("<div class='ok'><p>Validando seus dados... Aguarde um instante</p></div>");
			},
			success: function(valor){
				if(valor.erro == 'sim'){
					$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
				}else{
					$('.resp').html('<div class="ok"><p>Erro não definido</p></div>');
					window.location.href = "frmListaCli.php";
				}
			},
			error: function(valor){
				$('.resp').html('<div class="erros"><p> ' + valor.getErro + ' </p></div>');
			}
			
		});
		}
		evento.preventDefault();
	});	
});
// FIM	
	
	
</SCRIPT>
		
				
	</body>
</html>
<?php 
require_once('../CamadaControle/Classes/Login.php');
require_once('../CamadaControle/Classes/Pedido.php');
$objLogin = new Login();
$objLogin->verificarLogado();
require_once('../CamadaControle/Classes/Valida.php');
require_once('../CamadaControle/Classes/Vendedor.php');
require_once('../CamadaControle/Classes/Transportadora.php');
require_once('../CamadaControle/Classes/Connection.php');

$objVendedor = new Vendedor();
$vendedores = $objVendedor->listarVendedores();
$nVendedores = count($vendedores);

$objTransportadora = new Transportadora();
$transportadoras = $objTransportadora->listarTransportadoras();
$nTransportadoras = count($transportadoras);
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		<link href="css/esteps.css" rel="stylesheet" type="text/css" />
	</head>

<?php 
?>
	<body>
		<div id="view">
			<header class="quadro">
				<h1>Novo Pedido</h1>
			</header>
			<form id='formulario' method="POST" enctype="multipart/form-data" name='formulario'>
				<ul id="progress">
					<li class="ativo">Selecione o Cliente e o Vendedor</li>
					<li>Condições, prazos e transporte</li>
					<li>Observações</li>
				</ul>
							
				<fieldset>
					<div class="resp"></div>
					<h2>Dados do Vendedor e do Cliente</h2>
					<select id="lSelect" name="vendedor">
						<option class="lOption" name='vendedor'>Selecione o Vendedor</option>
						<?php 
						for($step = 0; $step < $nVendedores; $step++ )
						echo"<option class='lOption'>" . $vendedores[$step]['nome'] . "</option>";						
						?>
					</select>
					<h3>CPF/CNPJ do Cliente</h3>
					<input name='cpf' type='text' id="thisInput" placeholder="CPF/CNPJ do cliente"><br>	
					<h3><a href='frmCadCli.php'>Ou volte e cadastre um novo cliente</a></h3>
					<input type="button" name="next1" class="next acao" value="Proximo" />
				</fieldset>
				<fieldset>
					<div class="resp"></div>
					<h2>Condições, prazos e transporte</h2>
					
					<select id="lSelect" name="transportadora">
						<option class="lOption">Selecione a Transportadora</option>
						<?php 
						for($step = 0; $step < $nTransportadoras; $step++ )
						echo"<option class='lOption'>" . $transportadoras[$step]['nome'] . "</option>";						
						?>
					</select>
					<h3>Condição de pagamento</h3>
					<input type="number" min='1' max='24' name="condicao" placeholder="Número de parcelas" />
					
					<h3>Previsão de Entrega</h3>
					<input name="previsao" type="date" name="data" />
					<input type="button" name="prev" class="prev acao" value="Anterior" />
					<input type="button" name="next2" class="next acao" value="Proximo" />
				</fieldset>
				<fieldset>
					<div class="resp"></div>
					<h2>Observações</h2>
					<textarea name='observacoes'></textarea>
					<input type="button" name="prev" class="prev acao" value="Anterior" />
					<input type="submit" name="Enviar" class="acao Enviar" value="Criar Pedido" />
				</fieldset>
			</form>			
			
		</div>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
		<script src="js/animations.js"></script>
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script>
//mascaras para campos

   $("#rg").mask("99.999.999-9");
   $("#cep").mask("99999-999");
   $("#telefone").mask("(99) 9999-9999");
</script>
<SCRIPT>
/* FUNÇÃO JQUERY PARA VALIDAR, ENVIAR FORMULARIOS E PASSO A PASSO DOS FORMULARIOS */
$(function(){
	var atual_fs, next_fs, prev_fs;
	var formulario = $('form[name=formulario]');

	function next(elem){
		atual_fs = $(elem).parent();
		next_fs = $(elem).parent().next();


		$('#progress li').eq($('fieldset').index(next_fs)).addClass('ativo');
		atual_fs.hide(300);
		next_fs.show(300);
	}


	$('.prev').click(function(){
		atual_fs = $(this).parent();
		prev_fs = $(this).parent().prev();


		$('#progress li').eq($('fieldset').index(atual_fs)).removeClass('ativo');
		atual_fs.hide(300);
		prev_fs.show(300);
	});
	
	$('input[name=next1]').click(function(){
		var array = formulario.serializeArray();
		if(array[0].value == 'Selecione o Vendedor' || array[1].value == ''){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[name=next2]').click(function(){
		var array = formulario.serializeArray();
		if(array[2].value == 'Selecione a Transportadora' || array[3].value == '' || array[4].value == ''){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
		$.ajax({
			type: 'post',
			url: '../CamadaControle/cadastrarPedido.php',
			data: {cadastrar: 'sim', campos: array},
			dataType: 'json',
			beforeSend: function(){
				$('.resp').html("<div class='ok'><p>Validando seus dados... Aguarde um instante</p></div>");
			},
			success: function(valor){
				if(valor.erro == 'sim'){
					$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
				}else{
					$('.resp').html('<div class="ok"><p>'+ valor.msg +'</p></div>');
					 window.location.href = "frmListarPedidosPendentes.php";
				}
			}
		});
		evento.preventDefault();
	});	
});


// FIM	
	
	
</SCRIPT>
		
				
	</body>
</html>
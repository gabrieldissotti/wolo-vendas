<?php 
require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

require_once('../CamadaControle/Classes/Connection.php');
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		<link href="css/esteps.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Cadastro de Vendedor</h1>
			</header>
			<form  id='formulario' class="frm-medio" method="POST" enctype="multipart/form-data" name='cliente_frm'>
				<ul id="progress">
				</ul>
							
				<fieldset>
					<div class="resp"></div>
					<h2>Informações de Vendedor</h2>
					<h3>Nome</h3>
					<input name='nome' id="nome" type='text' placeholder="Nome">
					<input style="width:37%;" type="submit" name="Enviar" class="acao Enviar" value="Cadastrar" />
				</fieldset>
			</form>			
			
		</div>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
		<script src="js/animations.js"></script>



<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script>
//mascaras para campos
</script>
<SCRIPT>
/* FUNÇÃO JQUERY PARA VALIDAR, ENVIAR FORMULARIOS E PASSO A PASSO DOS FORMULARIOS */
$(function(){
	var formulario = $('form[name=cliente_frm]');
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
		if(array[0].value == ''){
			$('.resp').html('<div class="erros"><p>preencha todos os campos corretamente</p></div>');
		}else{
			$.ajax({
			type: 'post',
			url: '../CamadaControle/cadastrarVendedor.php',
			data: {cadastrar: 'sim', campos: array},
			dataType: 'json',
			beforeSend: function(){
				$('.resp').html("<div class='ok'><p>Validando seus dados... Aguarde um instante</p></div>");
			},
			success: function(valor){
				if(valor.erro == 'sim'){
					$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
				}else{
					$('.resp').html('<div class="ok"><p>'+ valor.msg +'</p></div>');
					 window.location.href = "frmListarVendedores.php";
				}
			},
			error: function(){
				alert("erro");
			}
			
		});
		}
		evento.preventDefault();
	});	
});
// FIM	
	
	
</SCRIPT>
		
				
	</body>
</html>
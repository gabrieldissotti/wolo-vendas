


<?php

require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();


require_once('../CamadaControle/Classes/Administrador.php');
 
require_once('../CamadaControle/util.php');
$objConnection = new Connection();
$objAdministrador = new Administrador();
$objAdministrador->visualizarConta();
?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Wolo | MGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Configurações de Conta</h1>
			</header>
			<div  class="container" >
			<style>
				table th{
					text-indent:20px;
				}
			</style>
			     	<table class="highlight card-painel white hoverable z-depth-5" >
				        <thead>
					        <tr>
					            <th data-field = "id">Conta do Administrador</th><th></th>
					        <th><form method='POST' action='frmAlteraUser.php' ><Button type='submit' name='alterar' value='" . '' . "' class='btn-floating blue darken-1 waves-small waves-effect'><i class='material-icons'>edit</i></button></form></th>
					        </tr>
					    </thead>
		
					    <tbody>
					    	<?php
						    	echo "
									<tr><td>Nome Completo</td><td> " . $objAdministrador->getNome()  . " " . $objAdministrador->getSobrenome() . "</td></tr>
		
									<tr class='dif'><td>Senha</td><td> " .  $objAdministrador->getSenha() . "</td></tr>
		
									<tr><td>E-mail</td><td> " .  $objAdministrador->getEmail() . "</td></tr>
									
									<tr class='dif'><td>CPF/CNPJ</td><td> " .  mask($objAdministrador->getCpfCnpj()) . "</td>
									</tr>
								";
							?>
				    </table>
			   
			    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
				<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
			</div>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>




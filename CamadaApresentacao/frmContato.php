<?php 
require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

require_once('../CamadaControle/Classes/Connection.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		<link href="css/esteps.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Contato</h1>
			</header>
			<form  id='formulario' class="frm-medio" method="POST" enctype="multipart/form-data" name='cliente_frm' action="../CamadaControle/send-msg.php">
				<ul id="progress">
				</ul>
							
				<fieldset>
					<div class="resp"></div>
					<h2>Envie Uma Mensagem Para Equipe Técnica</h2>
					<h3>Mensagem:</h3>
					
					<textarea name='descricao' id="descricao" type='text' placeholder="Digite sua Mensagem" style='text-transform: none;height:100px;'> </textarea>
					<input style="width:37%;" type="submit" name="Enviar" class="acao Enviar" value="Enviar" />
				</fieldset>
			</form>			
			
		</div>
			<script src="js/animations.js"></script>



<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

</body>
</html>
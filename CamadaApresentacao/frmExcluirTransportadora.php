<?php 


require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();
if(isset($_POST['excluir'])){
	$id = $_POST['excluir'];
require_once('../CamadaControle/Classes/Connection.php');
require_once('../CamadaControle/Classes/Transportadora.php');
$objTransportadora = new Transportadora();
$objTransportadora->visualizarTransportadora($id);
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		<link href="css/esteps.css" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Excluir Vendedor(a) "<?php echo $objTransportadora->getNome();  ?>"</h1>
			</header>
			<form id="cliente_frm" class="frm-medio" method="POST" enctype="multipart/form-data" name='cliente_frm'>
				<ul id="progress">
				</ul>
				<fieldset>
					<a href="frmListarVendedores.php"><button style="position:absolute;right:10px;" type="text" name="voltar" class='btn-floating blue darken-1 waves-small waves-effect'><i class="material-icons">navigate_before</i></button></a>
					<div class="resp"></div>
					<h2>Deseja Realmente Excluir "<?php echo $objTransportadora->getNome();  ?>"?</h2>
					<input name='id' id="id" hidden value='<?php echo $id ?>'></input>
					<input name='nome' disabled id="nome" type='text' placeholder="Nome" value='<?php  echo $objTransportadora->getNome();  ?>'>
					<input style="width:37%;background-color:#ff5252 ;"  type="submit" name="Enviar" class="acao Enviar" value="Excluir" />
					</fieldset>
			</form>			
				
		</div>
<?php } ?>
<script src="js/animations.js"></script>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
<SCRIPT>
/* FUNÇÃO JQUERY PARA VALIDAR, ENVIAR FORMULARIOS E PASSO A PASSO DOS FORMULARIOS */
$(function(){
	var formulario = $('form[name=cliente_frm]');
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
			$.ajax({
			type: 'post',
			url: '../CamadaControle/excluirTransportadora.php',
			data: {excluir: 'sim', campos: array},
			dataType: 'json',
			beforeSend: function(){
				$('.resp').html("<div class='ok'><p>Validando dados... Aguarde um instante</p></div>");
			},
			success: function(valor){
				if(valor.erro == 'sim'){
					$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
				}else{
					$('.resp').html('<div class="ok"><p>'+ valor.msg +'</p></div>');
					 window.location.href = "frmListarTransportadoras.php";
				}
			},
			error: function(){
				$('.resp').html('<div class="erros"><p>' + valor.getErro + '</p></div>');
			}
			
		});
		
		evento.preventDefault();
	});		
});
// FIM	
</SCRIPT>
		
				
	</body>
</html>
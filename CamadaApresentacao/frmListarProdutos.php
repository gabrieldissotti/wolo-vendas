<?php 


require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();


	  require_once('../CamadaControle/Classes/Produto.php');
 
$objConnection = new Connection();
$objLogin = new Login();

$objProduto = new Produto();
$produtos = $objProduto->listarProdutos();
$nprodutos = count($produtos);
?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Wolo | MGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Produtos</h1>
			</header>
			<div  class="container" >
			<style>
				table th{
					text-indent:20px;
				}
			</style>
			     	<table class="highlight card-painel white hoverable z-depth-5" >
				        <thead>
					        <tr>
					            <th data-field = "id">Classe</th>
					            <th data-field = "id">Descrição</th>
					            <th data-field = "id">Valor Unitário</th>
					        </tr>
					    </thead>
		
					    <tbody>
					    	<?php
					    	if(isset($produtos[0]['classe'])){
					    	for($step = 0; $step < $nprodutos; $step++){
						    	echo "
									<tr>
									<td> "  . $produtos[$step]['classe'] . " </td> 
									<td> "  . $produtos[$step]['descricao'] . " </td> 
									<td> "  . number_format($produtos[$step]['valorUnitario'], 2, ',', ' ') . " </td> 									
					            	<td><form method='POST' action='frmAlterarProduto.php' ><Button type='submit' name='alterar' value='" . $produtos[$step]['idProduto'] . "' class='btn-floating blue darken-1 waves-small waves-effect'><i class='material-icons'>edit</i></button></form></td>
					       			<td><form method='POST' action='frmExcluirProduto.php' ><Button type='submit' name='excluir' value='" . $produtos[$step]['idProduto'] . "' class='btn-floating   red accent-2 darken-1 waves-small waves-effect'><i class='material-icons'>delete_forever</i></button></form></td>
					       			</tr>
								";
							}}
					    	?>
					    	<?php 
							if(!isset($produtos[0]['classe'])){
											echo "<tr class='erros'><td colspan='7'>Não há produtos cadastrados</td><td><i class='material-icons''>warning</i></td></tr>";
							
							}
							?> 
					    </tbody>
				    </table>
				     <ul class="pagination card-painel white container" style='text-align: center; padding-top:10px;padding-bottom:10px;border-radius:30px;'>
						    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
						    <li class="active"><a href="#!">1</a></li>
						    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
					</ul>
			   
			    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
				<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
			</div>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>





<?php 

require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

	  require_once('../CamadaControle/Classes/Pedido.php');
	  require_once('../CamadaControle/Classes/Administrador.php');
	  require_once('../CamadaControle/Classes/Vendedor.php');
	  require_once('../CamadaControle/Classes/Transportadora.php');
	  require_once('../CamadaControle/Classes/Produto.php');
	  require_once('../CamadaControle/Classes/Cliente.php');
 
$objPedido = new Pedido();


?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/quadroFrm.css" />
		
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Novo Pedido</h1>
			</header>	
			<?php 
			$objPedido->listarPedidosAndamento();
			?>			
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>

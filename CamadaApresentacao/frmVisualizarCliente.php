
<?php

require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

	  require_once('../CamadaControle/Classes/Cliente.php');
 
$objConnection = new Connection();

$objCliente = new Cliente();
include_once('../CamadaControle/util.php');	


if(isset($_POST["visualizar"])){
		$id = $_POST["visualizar"];
		$objCliente->visualizarCliente($id);
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">

		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
	</head>

	<body>
		<style>
				table th{
					text-indent:20px;
				}
			</style>
		<div id="view">
			<header class="quadro">
				<h1><?php echo"Informações de {$objCliente->getNome()} {$objCliente->getSobrenome()}" ?></h1>
			</header>
			<div class=" container">
			<table class="highlight card-painel white hoverable z-depth-5 responsivo-table" >
				        <thead>
					        <tr>
					            <th data-field = "campo">Ficha de Cadastro</th>
					            <th data-field = "campo"></th>
					            <?php echo"
				<th data-field = 'alterar'>
				<a href='frmListaCli.php' ><button name='voltar' class='btn-floating grey darken-1 waves-small waves-effect inline-block'><i class='material-icons'>navigate_before</i></button></a>
				<form method='POST' action='frmAlterarCliente.htm.php' class='inline-block' ><Button type='submit' name='alterar' value='" . $id . "' class='btn-floating blue darken-1 waves-small waves-effect inline-block'><i class='material-icons'>edit</i></button></form>
				</th>
				  
					            
					             "?>
					            <th data-field = "price"></th>
					        </tr>
					    </thead>
		
					    <tbody>
<?php
	
		echo "
				<tr><td>Nome Completo</td><td> " . $objCliente->getNome()  . " " . $objCliente->getSobrenome() . "</td>
				<tr><td>CPF/CNPJ</td><td> " .  mask($objCliente->getCpfCnpj()) . "</td>
				<tr><td>RG</td><td> " .  $objCliente->getRg() . "</td>
				<tr><td>CEP</td><td> " .  $objCliente->getCep() . "</td>
				<tr><td>Bairro</td><td> " .  $objCliente->getBairro() . "</td>
				<tr><td>Endereço</td><td> " .  $objCliente->getEndereco() . "</td>
				<tr><td>Número</td><td> " .  $objCliente->getNumero() . "</td>
				<tr><td>Estado</td><td> " .  $objCliente->getEstado() . "</td>
				<tr><td>Complemento</td><td> " .  $objCliente->getComplemento() . "</td>
				<tr><td>E-mail</td><td> " .  $objCliente->getEmail() . "</td>
				<tr><td>Telefone</td><td> " .  mask($objCliente->getTelefone()) . "</td>
				   			
			
		";
	}
?>	
					    </tbody>
				    </table>
				    </div>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="js/animations.js"></script>
	</body>
</html>

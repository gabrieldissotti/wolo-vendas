<?php


require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();


require_once('../CamadaControle/Classes/Pedido.php');
require_once('../CamadaControle/Classes/Vendedor.php');
require_once('../CamadaControle/Classes/Transportadora.php');
require_once('../CamadaControle/Classes/Cliente.php');
 

 
$objConnection = new Connection();
$objLogin = new Login();

$objPedido = new Pedido(); 
$pedido = $objPedido->visualizarPedido($_POST['visualizar']);


?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Pedido em Andamento</h1>
			</header>
			<div  class="container" >
			<style>
				table th{
					text-indent:20px;
				}
			</style>
			     	<table class="highlight card-painel white hoverable z-depth-5" >
				        <thead>
					        <tr>
					            <th data-field = "classe">Classe</th>
					            <th data-field = "quantidade">Qtd</th>
					            <th data-field = "descricao">Descrição</th>
					            <th data-field = "precounitario">Unidade</th>
					            <th data-field = "subtotal">Subtotal</th>
					            <th data-field = "pronto"><form method='POST' name='rematar' action='../CamadaControle/cadastrarPedido.php'><input hidden name="rematarid"  value="<?php echo $_POST['visualizar'] ?>"><Button type='submit' name='rematar' class='btn-floating   green waves-small waves-effect'><i class="material-icons">gavel</i></button></form></th>
					          	<th data-field = "addloja"><form method='POST' name='cancelar' action=''><input hidden name="idpedido"  value="<?php echo $_POST['visualizar'] ?>"><Button type='submit' name='visualizar' class='btn-floating   red accent-2 waves-small waves-effect'><i class="material-icons">clear</i></button></form></th>
					           
					        </tr>
					    </thead>
		
					    <tbody>
					    	<?php
					    	for($step = 0; $step < count($pedido); $step ++){
						    	if(isset($pedido[$step]['idPedido'])){echo "
									<tr>
									<td> " . $pedido[$step]['classe'] . "</td>
									<td> " . $pedido[$step]['quantidade'] . "</td> 
									<td > " . $pedido[$step]['descricao'] . "x</td>
									<td > R$ " . number_format($pedido[$step]['valorUnitario'], 2, ',', ' ') . "</td> 
					            	<td > R$ " . number_format($pedido[$step]['valorUnitario'], 2, ',', ' ') . "</td> 
					        
					            	</tr>
								"; //  delete NÃO FUNCIONAL
								}
								}
							?>
							<?php 
							if(!isset($pedido[0]['idPedido'])){
											echo "<tr class='erros'><td colspan='8'>Não há Produtos no carrinho</td></tr>";
							}
							?> 
					    </tbody>
					   
				    </table>
				     <ul class="pagination card-painel white container" style='text-align: center; padding-top:10px;padding-bottom:10px;border-radius:30px;'>
						    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
						    <li class="active"><a href="#!">1</a></li>
						    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
					
					</ul>
					
			    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
				<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
			</div>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>



p
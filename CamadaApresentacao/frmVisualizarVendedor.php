
<?php


require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

	  require_once('../CamadaControle/Classes/Vendedor.php');
 
$objConnection = new Connection();
$objLogin = new Login();

$objVendedor = new Vendedor();
include_once('../CamadaControle/util.php');	

?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		
	</head>

	<body>
		<div id="view">
			<header class="quadro">
				<h1>Configurações</h1>
			</header>
<?php
	if(isset($_POST["+"])){
		$id = $_POST["+"];
		$objVendedor->visualizarVendedor($id);
		echo "
			<table>
				<caption>Informações de Conta</caption>
				<tr class='dif'><td>Nome Completo</td><td> " . $objVendedor->getNome()  . "</td>
				
			</table>
			<form method='POST' action='frmAlterarVendedor.php'><button class='btnAlterar' value='" . $id . "' name='Alterar'>Alterar</button></form>
		";
	}
?>	
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>

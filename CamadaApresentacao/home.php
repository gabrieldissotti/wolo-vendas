<!DOCTYPE html>
<?php require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

$objConnection = new Connection();
?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Wolo | MGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="midia/house.png">
		<link rel="apple-touch-icon" href="midia/house.png">
		<!-- Compiled and minified JavaScript -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		 <!--Import Google Icon Font-->
      	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     
	</head>
	<body onload="startCountdown();">
		
	
  
		<div id="view">	
			
			<div id="preloader">
				<img src="midia/preloader.gif">
				<p>Carregando...</p>
				<p id="aviso">Acaso o carregamento esteja demorando muito, verifique sua conexão com a internet.</p>
			</div>	
			<!-- Switch -->
		  		
			<div id="desligar">
				<button id="fechar" onclick="aparecer('desligar');"> </button>
			
				<figure>
					<img src="midia/warning.png" />
				</figure>
				<p>Tem certeza que deseja encerrar a sessão?</p>
				<div class="msg-btns">
					<FORM method="POST">
					<button id="sim" class="btn"  value="Encerrar" name="Encerrar">Encerrar</button>
					</FORM> 
					<?php if(isset($_POST["Encerrar"]) && $_POST["Encerrar"] == "Encerrar"){ $objLogin->deslogar();
			           }
			        ?>
		     	   <button id="nao" class="btn"  onclick="aparecer('desligar');">Cancelar</button>
				</div>
			</div>
			<div id="renovar">
				<button id="fechar" onclick="aparecer('renovar')"> </button>
				<figure>
					<img src="midia/warning.png" />
				</figure>
				<p>A sessão atual será finalizada em <span class="sessao"></span>.<br> Deseja renovar a sessão?</p>
				<div class="msg-btns">
					<FORM method="POST">
					<button id="sim" class="btn" value="Renovar" name="Renovar">Renovar</button>
					
					</FORM> 
					<?php if(isset($_POST["Renovar"]) && $_POST["Renovar"] == "Renovar"){ $objLogin->renovar();
			        }
			        ?>
			        <button id="nao" class="btn"  onclick="aparecer('renovar')">Cancelar</button>
				</div>
			</div>
			<span id="box-toggle">
				<header>
					<a href="#" data-activates="slide-out" class="button-collapse"><img src="midia/settings.png" id='set'/></a>
					<h1><a style="color:white;" href="home.php">Wolo</a> <span id="tosigla">Módulo de Gerenciamento de Vendas</span></h1>
					<figure onclick="aparecer('desligar');">
						<img src="midia/power-button.png"/>
					</figure>
					<figure>
						<img src="midia/menu-3.png"  class="m" />
						<ol class="tgl" id="menu">
							<li onclick="aparecer('menu');atiframe('frmConfig.php');" id="x0">Configurações de Conta</li>
				
							<li onclick="aparecer('menu');atiframe('frmContato.php');" id="x1">Suporte técnico</li>
							<li onclick="aparecer('menu');atiframe('frmContato.php');" id="x3">Reportar bug</li>
							
							<li onclick="aparecer('renovar');" id="x2">A sessão expira em <span class="sessao"></span></li>	
						
						</ol>
					</figure>
					
				</header>
				<nav id="slide-out"  class="side-nav"  style="position:absolute;background-color:transparent;box-shadow:none;margin-top:50px;">
					<ol id="an">
						<li id="hg1" class="m">Clientes</li>
						<ul class="tgl" id="u1">
							<li id="a2" class="esconder" onclick="atiframe('frmListaCli.php');">Mostrar Todos</li>
							<li id="a1"  class="esconder" onclick="atiframe('frmCadCli.htm.php');">Cadastrar</li>					
						</ul>
						<li id="hg2" class="m">Vendedores</li>
						<ul class="tgl" id="u2">	
							<li id="b1"  class="esconder" onclick="atiframe('frmListarVendedores.php');">Mostrar Todos</li>
							<li id="b2"  class="esconder" onclick="atiframe('frmCadVendedor.php');">Cadastrar</li>				
						</ul>	
						<li id="hg3" class="m">Produtos</li>
						<ul class="tgl" id="u3">
							<li id="cc1"  class="esconder" onclick="atiframe('frmListarProdutos.php');">Listar Todos</li>
							<li id="cc4"  class="esconder" onclick="atiframe('frmCadProdutos.php');">Adicionar</li>					
						</ul>
						<li id="hg4" class="m">Transportadoras</li>
						<ul class="tgl" id="u4">
							<li id="dd1"  class="esconder" onclick="atiframe('frmListarTransportadoras.php');">Mostrar Todas</li>
							<li id="dd2"  class="esconder" onclick="atiframe('frmCadTransportadora.php');">Cadastrar</li>					
						</ul>	
						<li id="hg5" class="m">Pedidos</li>
						<ul class="tgl" id="u5">
							<li id="e1"  class="esconder" onclick="atiframe('frmListarPedidosRematados.php');">Rematados</li>
							<li id="e2"  class="esconder" onclick="atiframe('frmListarPedidosAndamento.php');">Em Andamento</li>
							<li id="e3"  class="esconder" onclick="atiframe('frmListarPedidosPendentes.php');">Pendentes</li>
							<li id="e4"  class="esconder" onclick="atiframe('frmCadPedido.htm.php');">Novo</li>
												
						</ul>	
					</ol>
				</nav>
				<textarea type="text" style="background-color:rgba(255,255,255,.3);border-radius:10px; width:50%;margin-left:25%;margin-right:25%;position:absolute;z-index:1000;bottom:0;color:rgb(0,0,0.4);max-width:50%;max-height:40px; padding:5px;font-size:14pt;font-family: Roboto; text-align:center;" hidden name="campo_texto" id="campo_texto" /></textarea>
				
				
				<iframe src="novidades.php" id="atividades"></iframe>
				
				<script src="js/animations.js"></script>
			</span>
			<!--<input type="text" name="text"/>
			<a href="#" class"say">Fale isso!</a>
			<audio hidden class="speech"  src="" controls></audio>﻿	
			-->
		</div>	
		<div class="fixed-action-btn horizontal" >
				    <a class="btn-floating btn-large blue">
				    <i class="material-icons">apps</i>
				    </a>
				    <ul style="background-color:transparent;margin:0;">
				      <li style="border:none;margin:0;padding:5px;filter:invert(0%);"><a class="btn-floating grey" id="voice_on_off"><i class="material-icons" id="btn_gravar_audio">keyboard_voice</i></a></li>
				      <li style="border:none;margin:0;padding:5px;filter:invert(0%);"><a onclick="atiframe('manualvoice.php');" class="btn-floating blue accent-2" id="voice_on_off"><i class="material-icons">help_outline</i></a></li>
				    		
				    	
				    </ul>
				  </div>
		<!-- <script>
			//função de voz
			$(function() {
				$('a.say').on('click', function(e){
					e.preventDefault();
					var text = $('input[name="text"]').val();
					text = encodeURIComponent(text);
					console.log(text);
					var url = "https://translate.google.com/translate_tts?ie=UTF-&&q=" + text + "&tl=en";
					$('audio').attr('src', url).get(0).play();
				});
			});
		</script>
	-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>         

	
	
	<script src="js/voice.js"></script>
<script>
$('.button-collapse').sideNav({
  menuWidth: 280, // Default is 240
  closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
}
  );
  $('.collapsible').collapsible();
  $('.esconder').click( function(){
  	$('.button-collapse').sideNav('hide');
  	});
</script>
		<script type="text/javascript"> 
			jQuery.fn.toggleText = function(a,b) {
				return   this.html(this.html().replace(new RegExp("("+a+"|"+b+")"),function(x){return(x==a)?b:a;}));
			}
			$(document).ready(function(){
				$('.tgl').before('');
				$('.tgl').css('display', 'none')
				$('.m', '#box-toggle').click(function() {
					$(this).next().slideToggle('slow')
					.siblings('.tgl:visible').slideToggle('fast');
					// aqui começa o funcionamento do plugin
					$(this).toggleText('Revelar','Esconder')
					.siblings('span').next('.tgl:visible').prev()
					.toggleText('Revelar','Esconder')
				});
			})
		</script>
		<script>
			$(document).ready(function(){
			    //Esconde preloader
			    $(window).load(function(){
			        $('#preloader').fadeOut(1500);//1500 é a duração do efeito (1.5 seg)
			    });
			});
		</script>
		<script>
		//responsividade de h1 do header
$(document).ready(function(){
			var scre = $(window).width();
			var tosigla = document.getElementById('tosigla');
		    if ( scre >= 588 ) {
		        tosigla.innerHTML = "Módulo de Gerenciamento de Vendas";
		    } if ( scre < 588) {
		        tosigla.innerHTML = "MGV";
		    }
	 $(window).resize(function() {
	        // Adicionar sempre que a tela for redimensionada
	        var scre = $(window).width();
			var tosigla = document.getElementById('tosigla');
		    if ( scre >= 588 ) {
		        tosigla.innerHTML = "Módulo de Gerenciamento de Vendas";
		    } if ( scre < 588) {
		        tosigla.innerHTML = "MGV";
		    }
	 });
});
		</script>
		
	</body>
</html>

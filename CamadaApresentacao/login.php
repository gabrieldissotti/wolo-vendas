<!DOCTYPE html>
<?php require_once('../CamadaControle/Classes/Login.php');
 
$objConnection = new Connection();
$objLogin = new Login();
?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Entrar | MGV</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/login.css" />
		
	</head>

	<body>
		<div id="view">
			<header>
				<h1>Entrar <span id="tosigla">MGV</span></h1>
			</header>
			<img src='midia/busi.png' class='backimg'>
			<div id="view-center">
				<figure id="view-center-img">
					<img src="midia/user.png"/>
				</figure>
				<form action="" method="POST">
	               <label for="email">E-mail:</label><br>
	                
	               <input type="text" name="email" id="email" required/><br>
	                
	                
	               <label for="senha">Senha:</label><br>
	                
	               <input type="password" name="senha" id="senha" required/><br>
	                
	                
	               <input type="submit" value="Entrar" name="Enviar" id="env"/>
	            </form>	
		        <?php if(isset($_POST["Enviar"]) && $_POST["Enviar"] == "Entrar"){ $logar = $objLogin->Logar($_POST["email"],$_POST['senha']);
		           }
		        ?>
		         
		        <?php if (isset($logar)){ ?>
		 
				<div class="container-erro">
		        	<?php echo $logar ?>
		        </div>
		 
		    	<?php } ?>
			</div>
			<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
	
<script>
$(document).ready(function(){
	 var scre = $(window).width();
			var viewCenter = document.getElementById('view-center');
	        var screy = $(window).height();
		    if ( scre >= 776 ) {
		        viewCenter.style.width = "350px";
		    } if ( scre < 776) {
		        viewCenter.style.width = "100%";
		    }
		    if(screy < 549){
		    	viewCenter.style.paddingTop = "10px";
		    }
		    else{
		    	viewCenter.style.paddingTop = "100px";
		    }
	 $(window).resize(function() {
	        // Adicionar sempre que a tela for redimensionada
	        var scre = $(window).width();
			var viewCenter = document.getElementById('view-center');
	        var screy = $(window).height();
		    if ( scre >= 776 ) {
		        viewCenter.style.width = "350px";
		    } if ( scre < 776) {
		        viewCenter.style.width = "100%";
		    }
		    
		    if(screy < 549){
		    	viewCenter.style.paddingTop = "10px";
		    }
		    else{
		    	viewCenter.style.paddingTop = "100px";
		    }
	 });
});
</script>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>

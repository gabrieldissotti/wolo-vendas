<!DOCTYPE html>
<?php

require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

require_once('../CamadaControle/Classes/Pedido.php');
require_once('../CamadaControle/Classes/Vendedor.php');
require_once('../CamadaControle/Classes/Transportadora.php');
require_once('../CamadaControle/Classes/Cliente.php');
 
$objConnection = new Connection();

$objPedido = new Pedido(); 
$pedidos = $objPedido->listarPedidosAndamento();
$npedidos = count($pedidos);
?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	</head>

	<body>
		<div id="view">
			<!--<header class="quadro">
				<h1>Pedidos em Andamento</h1>
			</header>-->
			<div  class="container" >
			<style>
				table th{
					text-indent:20px;
				}
			</style>
			     	<table class="highlight card-painel white hoverable z-depth-5" >
				        <thead>
					        <tr>
					        	<th data-field = "price">Comando de Voz</th>
					            <th data-field = "vendedor">Descrição da Tarefa</th>
					            <th data-field = "vendedor">Outrros Comandos</th>
					       			
					        </tr>
					    </thead>
		
					    <tbody>
					    	
							<tr>
								<td>Novo Pedido</td>
								<td>Trás o formulário de cadastro de um novo pedido</td>
			            		<td>Cadastrar Pedido</td>
			            	</tr>
			            	<tr>
								<td>Novo Cliente</td>
								<td>Trás o formulário de cadastro de um novo Cliente</td>
								<td>Cadastrar Cliente</td>
			            	</tr>
			            	<tr>
								<td>Novo Vendedor</td>
								<td>Trás o formulário de cadastro de um novo Vendedor</td>
								<td>Cadastrar Vendedor <br> Nova Vendedora <br> Cadastrar Vendedora</td>
			            	</tr>
			            	<tr>
								<td>Mostrar Clientes</td>
								<td>Exibe todos os clientes cadastrados</td>
								<td>Visualisar Clientes<br> Ver Clientes <br> Listar Clientes </td>
			            	</tr>
			            	<tr>
								<td>Pedidos Pentendes</td>
								<td>Exibe todos os clientes cadastrados</td>
								<td>Pedido Pentente</td>
			            	</tr>
					    </tbody>
				    </table>
			</div>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>




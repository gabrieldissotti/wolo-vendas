<!DOCTYPE html>
<?php

require_once('../CamadaControle/Classes/Login.php');
$objLogin = new Login();
$objLogin->verificarLogado();

require_once('../CamadaControle/Classes/Pedido.php');
require_once('../CamadaControle/Classes/Vendedor.php');
require_once('../CamadaControle/Classes/Transportadora.php');
require_once('../CamadaControle/Classes/Cliente.php');
 
$objConnection = new Connection();

$objPedido = new Pedido(); 
$pedidos = $objPedido->listarPedidosAndamento();
$npedidos = count($pedidos);
?>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>SGMP</title>
		<meta name="description" content="SGMP">
		<meta name="author" content="Gabriel Dissotti">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="apple-touch-icon" href="../CamadaApresentacao/midia/user-1.png">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="../CamadaApresentacao/css/quadroFrm.css" />
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
	</head>

	<body>
		<div id="view">
			<!--<header class="quadro">
				<h1>Pedidos em Andamento</h1>
			</header>-->
			<div  class="container" >
			<style>
				table th{
					text-indent:20px;
				}
			</style>
			     	<table class="highlight card-painel white hoverable z-depth-5" style='text-align:center;'>
				        <thead>
					        <tr>
					        	<th data-field = "price" style='text-align:center;'>Novidades</th>
					        </tr>
					    </thead>
		
					    <tbody>
					    	
							<tr>
								<td style='text-align:center;'>Experimente utilizar de comandos por voz para acessar os painéis princípais</td>
			            	</tr>
			            	<tr>
								<td style='text-align:center;'>Algum problema? Envie-nos uma mensagem pelo próprio Sistema</td>
			            	</tr>
					    </tbody>
				    </table>
			</div>
		</div>
		<script src="js/animations.js"></script>
	</body>
</html>




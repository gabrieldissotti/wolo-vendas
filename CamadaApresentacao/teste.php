<?php
require_once('../CamadaControle/Classes/Login.php');
require_once('../CamadaControle/Classes/Cliente.php');
require_once('../CamadaControle/Classes/Connection.php');
require_once('../CamadaControle/Classes/Valida.php');
if(isset($_POST['cadastrar']) && $_POST['cadastrar'] == 'sim'){
	$novos_campos = array();
	$campos_post = $_POST['campos'];
	$respostas = array();
	
	foreach ($campos_post as $indice => $valor) {
		$novos_campos[$valor['name']] = $valor['value'];
	}
	$objValida = new ValidaCPFCNPJ($novos_campos['cpf']);
	$validado = $objValida->valida();
	if($validado == false){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'CPF/CNPJ Inválido, preencha com CPF/CNPJ Válido';			
	}elseif(!preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $novos_campos['cep'])){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'CEP inválido, Digite um CEP Válido';	
	}elseif(!preg_match('^\(+[0-9]{2,3}\) [0-9]{4}-[0-9]{4}$^', $novos_campos['telefone'])){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'Telefone inválido, Digite um telefone Válido';	
	}elseif(!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $novos_campos['email'])){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'E-mail inválido, Digite um E-mail Válido';	
	}
	else{
		$objCliente = new Cliente();
		$novos_campos = array(
		'nome' => 'gabriela'
		);
		$objCliente->cadastrarCliente($novos_campos);
		
		$respostas['erro'] = 'nao';
		$respostas['msg']  = 'Salvando Dados...';		
	}
}
?>
<?php
	class Administrador{
		private $idAdministrador;
		private $senha;
		private $email;
		private $cpfCnpj;
		private $nome;
		private $sobrenome;
		
		
		
		function __construct(){
	      $objConnection = new Connection();
	   	}
		
		public function setId($id){
			$this->idAdministrador = $id;
		}
		public function setSenha($senha){
			$this->senha = $senha;
		}
		public function setEmail($email){
			$this->email = $email;
		}
		public function setCpfCnpj($cpfCnpj){
			$this->cpfCnpj = $cpfCnpj;
		}
		public function setNome($nome){
			$this->nome= $nome;
		}
		public function setSobrenome($sobrenome){
			$this->sobrenome = $sobrenome;
		}
		
		public function getId(){
			return($this->idAdministrador);
		}
		public function getSenha(){
			return($this->senha);
		}
		public function getEmail(){
			return($this->email);
		}
		public function getCpfCnpj(){
			return($this->cpfCnpj);
		}
		public function getNome(){
			return($this->nome);
		}
		public function getSobrenome(){
			return($this->sobrenome);
		}
		public function visualizarConta(){
			
		    $objConnection = new Connection();
			$Administrador = $objConnection->DBRead("Administrador");
	     	
						
			include_once('../CamadaControle/util.php');	
			$this->idAdministrador = $Administrador[0]['idAdministrador'];			
			$this->nome = $Administrador[0]['nome'];
			$this->sobrenome = $Administrador[0]['sobrenome'];
			$this->senha = $Administrador[0]['senha'];
			$this->email = $Administrador[0]['email'];
			$this->cpfCnpj = $Administrador[0]['cpfCnpj'];
		}
		
		public function alterarConta(){
			
		    $objConnection = new Connection();
			$adm = array(
				'nome' 		=> $this->nome,
				'sobrenome' => $this->sobrenome,
				'email' 	=> $this->email,
				'senha' 	=> $this->senha,
				'cpfCnpj' 	=> $this->cpfCnpj,	
			);
			
			$Administrador = $objConnection->DBUpdate("Administrador", $adm);
			header("location:frmConfig.php");
		}
	}
?>
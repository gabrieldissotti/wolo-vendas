<?php
	class Cliente{
		private $idCliente;
		private $email;
		private $nome;
		private $endereco;
		private $bairro;
		private $cidade;
		private $cpfCnpj;
		private $sobrenome;
		private $rg;
		private $telefone;
		private $cep;
		private $estado;
		
		private $complemento;
		private $numero;
		
		
		
		
		function __construct(){
	      $objConnection = new Connection();
	   	}
		
		public function setId($id){
			$this->idCliente = $id;
		}
		public function setEmail($email){
			$this->email = $email;
		}
		public function setCpfCnpj($cpfCnpj){
			$this->cpfCnpj = $cpfCnpj;
		}
		public function setNome($nome){
			$this->nome= $nome;
		}
		public function setSobrenome($sobrenome){
			$this->sobrenome = $sobrenome;
		}
		public function setEndereco($endereco){
			$this->endereco = $endereco;
		}
		public function setCidade($cidade){
			$this->cidade = $cidade;
		}
		public function setRg($rg){
			$this->rg = $rg;
		}
		public function setTelefone($telefone){
			$this->telefone = $telefone;
		}
		public function setCep($cep){
			$this->cep = $cep;
		}
		public function setEstado($estado){
			$this->estado = $estado;
		}
		public function setBairro($bairro){
			$this->bairro = $bairro;
		}
		
		
		public function setComplemento($complemento){
			$this->complemento = $complemento;
		}
		
		public function setNumero($numero){
			$this->numero = $numero;
		}
		
		
		//gets
		
		public function getId(){
			return($this->idCliente);
		}
		public function getEmail(){
			return($this->email);
		}
		public function getCpfCnpj(){
			return($this->cpfCnpj);
		}
		public function getNome(){
			return($this->nome);
		}
		public function getSobrenome(){
			return($this->sobrenome);
		}
		
		public function getEndereco(){
			return $this->endereco;
		}
		public function getCidade(){
			return $this->cidade;
		}
		public function getRg(){
			return $this->rg;
		}
		public function getTelefone(){
			return $this->telefone;
		}
		public function getCep(){
			return $this->cep;
		}
		public function getEstado(){
			return $this->estado;
		}
		public function getBairro(){
			return $this->bairro;
		}
		public function getComplemento(){
			return $this->complemento;
		}
		
		public function getNumero(){
			return $this->numero;
		}
		
		
		//metodos	
		//RIGHT
		public function listarClientes(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
		    $query = $objConnection->DBRead("Cliente","", "idCliente, nome, sobrenome, telefone, email");
	     	return $query;			
		}
		
		//RIGHT
		public function visualizarCliente($id){
			
		    $objConnection = new Connection();
			$clientes = $objConnection->DBRead("Cliente","WHERE idCliente={$id}", "*");
	     	
			$this->idCliente = $clientes[0]['idCliente'];			
			$this->nome = $clientes[0]['nome'];
			$this->sobrenome = $clientes[0]['sobrenome'];
			$this->email = $clientes[0]['email'];
			$this->cpfCnpj = $clientes[0]['cpfCnpj'];
		   	$this->endereco = $clientes[0]['endereco'];
		   	$this->bairro = $clientes[0]['bairro'];
		   	$this->cidade = $clientes[0]['cidade'];
		   	$this->rg = $clientes[0]['rg'];
		   	$this->telefone = $clientes[0]['telefone'];
		   	$this->cep = $clientes[0]['cep'];
		   	$this->estado = $clientes[0]['estado'];	
		   	$this->numero = $clientes[0]['numero'];	
		   	$this->complemento = $clientes[0]['complemento'];			
		}
		
		
		
		#VERIFICAR
		public function pesqCliente($nomeOuCpfCnpj){
			$objConnection = new Connection();
			$clientes = $objConnection->DBRead("Cliente","WHERE nome like '%{$nomeOuCpfCnpj}%' or cpfCnpj like '%{$nomeOuCpfCnpj}%'", "*");
			return $clientes;
		}
		
		//RIGHT
		public function alterarCliente($id){
			$nome=$this->nome;
			$sobrenome=$this->sobrenome;
			$email=$this->email;
			$cpfCnpj=$this->cpfCnpj;
		   	$endereco=$this->endereco;
		   	$bairro=$this->bairro;
		   	$cidade=$this->cidade;
		   	$rg=$this->rg;
		   	$telefone=$this->telefone;
		   	$cep=$this->cep;
		   	$estado=$this->estado;	
		   	$numero=$this->numero;	
		   	$complemento=$this->complemento;		
			
			$data = array(
			'nome' 			=> $nome,
			'sobrenome'		=> $sobrenome,
			'email'			=> $email,
			'cpfCnpj'		=> $cpfCnpj,
			'telefone'		=> $telefone,
			'endereco'		=> $endereco,
			'cep'			=> $cep,
			'numero'		=> $numero,
			'complemento'	=> $complemento,
			'bairro'		=> $bairro,
			'cidade'		=> $cidade,
			'rg'			=> $rg,
			'estado'		=> $estado
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$clientes = $objConnection->DBUpDate('Cliente', $data, "idCliente={$id}");
			
		}
		
		
		//RIGHT
		public function cadastrarCliente($campos){
			$nome 			= $campos['nome'];
			$sobrenome 		= $campos['sobrenome'];
			$email 			= $campos['email'];
			$cpfCnpj 		= preg_replace('[^0-9]', '', $campos['cpf']);
			$telefone		= preg_replace( '/[^0-9]/', '', $campos['telefone']);
			$endereco		= $campos['endereco'];
			$cep			= preg_replace( '/[^0-9]/', '', $campos['cep']);
			$numero			= preg_replace( '/[^0-9]/', '', $campos['numero']);
			$complemento	= $campos['complemento'];
			$bairro			= $campos['bairro'];
			$cidade			= $campos['cidade'];
			$rg				= preg_replace( '/[^0-9]/', '', $campos['rg']);
			$estado			= $campos['estado'];
			
			$nome = mb_strtoupper($nome);
			$sobrenome = mb_strtoupper($sobrenome);
			$endereco = mb_strtoupper($endereco);
			$complemento = mb_strtoupper($complemento);
			$bairro = mb_strtoupper($bairro);
			$cidade = mb_strtoupper($cidade);
			
			
			$data = array(
			'nome' 			=> $nome,
			'sobrenome'		=> $sobrenome,
			'email'			=> $email,
			'cpfCnpj'		=> $cpfCnpj,
			'telefone'		=> $telefone,
			'endereco'		=> $endereco,
			'cep'			=> $cep,
			'numero'		=> $numero,
			'complemento'	=> $complemento,
			'bairro'		=> $bairro,
			'cidade'		=> $cidade,
			'rg'			=> $rg,
			'estado'		=> $estado
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$objConnection->DBCreate('Cliente', $data);
		}





		#Right1
		public function verificarExistencia($cpf){
			$cpf = preg_replace("/[^0-9]/", "", $cpf);
			$objConnection = new Connection();
			$objConnection->Conectar();
			$cliente = $objConnection->DBRead("Cliente","WHERE cpfCnpj like '{$cpf}'",'*'); 
			if($cliente){
				return true;
			}
			else{
				return false;
			}
			
		}
		
		
		
		
	}
?>
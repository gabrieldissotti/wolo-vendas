<?php
	class Pedido{
		private $idPedido;
		private $condicao;
		private $observacao;
		private $administrador;
		private $cliente;
		private $vendedor;
		private $transportadora;
		private $total;
				
		function __construct(){
	      $objConnection = new Connection();
	   	}
		
		public function setObservacao($observacao){
			$this->observacao = $observacao;
		}
		public function setTotal($total){
			$this->total = $total;
		}
		public function setCondicao($condicao){
			$this->condicao = $condicao;
		}
		public function setAdministrador($administrador){
			$this->condicao = $condicao;
		}
		public function setTransportadora($transportadora){
			$this->transportadora = $transportadora;
		}
		public function setCliente($cliente){
			$this->cliente = $cliente;
		}
		public function setVendedor($vendedor){
			$this->vendedor = $vendedor;
		}
		
		
		//gets		
		public function getId(){
			return($this->idPedido);
		}
		public function getObservacao(){
			return($this->observacao);
		}
		public function getTotal(){
			return($this->total);
		}
		public function getCondicao(){
			return($this->condicao);
		}
		public function getTransportadora(){
			return($this->transportadora);
		}
		public function getAdministrador(){
			return($this->administrador);
		}
		public function getCliente(){
			return($this->cliente);
		}
		
		public function getVendedor(){
			return($this->vendedor);
		}
		
		
		//metodos		
		
		//RIGHT
		public function cadastrarPedido($campos){
		  	$objConnection = new Connection();
			
		    $cpf = ereg_replace('[^0-9]', '', $campos['cpf']);
			
			$vendedor = $campos['vendedor'];
			$transportadora = $campos['transportadora'];
			$condicao = $campos['condicao'];
			$observacoes = $campos['observacoes'];
			
			//UTILIZO OS DADOS VINDO DO FORMULARIO PARA BUSCAR O ID DAS ENTIDADES
			$idCliente = $objConnection->DBRead('Cliente',"WHERE Cliente.cpfCnpj={$cpf}",'idCliente');
			$idVendedor = $objConnection->DBRead('Vendedor',"WHERE Vendedor.nome like '%{$vendedor}%'",'idVendedor');
			$idTransportadora = $objConnection->DBRead('Transportadora',"WHERE Transportadora.nome like '%{$transportadora}%'",'idTransportadora');
			$data = array(
			'condicao' => $condicao,
			'observacao' => $observacoes,
			'estado' => 'P',
			'Cliente_idCliente' => $idCliente[0]['idCliente'],
			'Vendedor_idVendedor' => $idVendedor[0]['idVendedor'],
			'Transportadora_idTransportadora' => $idTransportadora[0]['idTransportadora']
			);
			
			$objConnection->DBCreate('Pedido', $data);
		}
		
		
		public function mostrarEstadoPedido($cliente){
			$objConnection = new Connection();
		    $objConnection->Conectar();
		    $query = $objConnection->DBRead("Pedido","", "*");
			$Pedidos = $query;
			echo "<tr class='dif'><th>Nome Completo</th><th>Telefone</th><th>E-mail</th></tr>
			<tr>
				<td> "  . $Pedidos[0]['idPedido'] . "</td> 
				<td><form method='POST' action='frmVisualizarCli.php'> <button class='btnAlterar ' value='" . $Pedidos[$step]['idPedido'] . "' name='+'>+</button></td></form>
				<td style='display:none'>" . $Pedidos[$step]['idPedido'] . "</td> 
			</tr>
			";			
		 }
		
		public function listarPedidosAndamento(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$query = $objConnection->DBRead("Pedido","INNER JOIN Cliente ON (Pedido.Cliente_idCliente = Cliente.idCliente) INNER JOIN Vendedor ON (Pedido.Vendedor_idVendedor=Vendedor.idVendedor) WHERE Pedido.estado like 'A'", "Cliente.nome as clientenome, Vendedor.nome as vendedornome, Pedido.condicao, Pedido.total, Pedido.idPedido");
	     	$pedidos = $query;
			return $pedidos;
		}
		
		
		//RIGHT
		public function listarPedidosPendentes(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$query = $objConnection->DBRead("Pedido","INNER JOIN Cliente ON (Pedido.Cliente_idCliente = Cliente.idCliente) INNER JOIN Vendedor ON (Pedido.Vendedor_idVendedor=Vendedor.idVendedor) WHERE Pedido.estado like 'P'", "Cliente.nome as clientenome, Vendedor.nome as vendedornome, Pedido.condicao, Pedido.total, Pedido.idPedido");
	     	$pedidos = $query;
			return $pedidos;
		}
		public function listarPedidosRematados(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$query = $objConnection->DBRead("Pedido","INNER JOIN Cliente ON (Pedido.Cliente_idCliente = Cliente.idCliente) INNER JOIN Vendedor ON (Pedido.Vendedor_idVendedor=Vendedor.idVendedor) WHERE Pedido.estado like 'R'", "Cliente.nome as clientenome, Vendedor.nome as vendedornome, Pedido.condicao, Pedido.total, Pedido.idPedido");
	     	$pedidos = $query;
			return $pedidos;
		}
		
		//RIGHT
		public function visualizarPedido($idPedido){
			$objConnection = new Connection();
		    $objConnection->Conectar();
			#$query = $objConnection->DBRead("Pedido p","INNER JOIN Pedido_has_Produto pp ON (p.idPedido = pp.Pedido_idPedido) INNER JOIN Produto pr ON (pr.idProduto = pp.Produto_idProduto)", "p.Cliente_idCliente, ");
	     	$query = $objConnection->DBRead("Pedido_has_Produto pp","INNER JOIN Pedido p ON (p.idPedido = pp.Pedido_idPedido) INNER JOIN Produto pr ON (pr.idProduto = pp.Produto_idProduto) WHERE p.idPedido like '{$idPedido}'", "p.idPedido, pr.idProduto, pr.quantidade, pr.classe, pr.descricao, pr.subtotal, pr.valorUnitario");
	     	$pedido = $query;
			return $pedido;
		}
		
		
		//TORNAR PERDIDO PENDENTE EM PEDIDO EM ANDAMENTO
		public function tornarEmAndamento($idPedido){
			$data = array(
			'estado' => 'A'
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$objConnection->DBUpDate('Pedido', $data,"idPedido={$idPedido}");			
			
		}
		public function rematarPedido($idPedido){
			$data = array(
			'estado' => 'R'
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$objConnection->DBUpDate('Pedido', $data,"idPedido={$idPedido}");			
			
		}
		
	}
?>

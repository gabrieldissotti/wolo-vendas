<?php
	class Produto{
		private $idProduto;
		private $classe;
		private $descricao;
		private $subtotal;
		private $valorUnitario;
		private $quantidade;
		
		
		
		
		function __construct(){
	      $objConnection = new Connection();
	   	}
		
		
		
		
		//sets
		public function setValorUnitario($vunitario){
			$this->valorUnitario = $vunitario;
		}
		public function setQuantidade($quantidade){
			$this->quantidade = $quantidade;
		}
		public function setId($id){
			$this->idProduto = $id;
		}
		public function setClasse($classe){
			$this->classe= $classe;
		}
		public function setDescricao($descricao){
			$this->descricao = $descricao;
		}
		public function setSubtotal($subtotal){
			$this->subtotal = $subtotal;
		}
		
		
		//gets
		public function getValorUnitario(){
			return $this->valorUnitario;
		}
		public function getQuantidade(){
			return $this->quantidade;
		}
		public function getId(){
			return($this->idProduto);
		}
		public function getClasse(){
			return($this->classe);
		}
		public function getDescricao(){
			return($this->descricao);
		}
		public function getSubtotal(){
			return($this->subtotal);
		}
		
		//metodos		
		public function listarProdutos(){
		    $objConnection = new Connection();
		    $objConnection->Conectar();
		    $query = $objConnection->DBRead("Produto","", "classe, descricao, valorUnitario, idProduto");
	     	return $query;
	    }
		
		
		public function visualizarProduto($id){
			
		    $objConnection = new Connection();
			$Produtos = $objConnection->DBRead("Produto","WHERE idProduto={$id}", "*");
	     	
			$this->idProduto = $Produtos[0]['idProduto'];			
			$this->classe = $Produtos[0]['classe'];	
			$this->descricao = $Produtos[0]['descricao'];	
			$this->subtotal = $Produtos[0]['subtotal'];
		}
		
		
		public function alterarProduto($id){
			$objConnection = new Connection();
			$data = array(
				'classe' 		=> $this->classe,
				'descricao' 	=> $this->descricao,
				'subtotal' 		=> $this->subtotal
			);
			$Produtos = $objConnection->DBUpdate('Produto', $data, "idProduto={$id}");
			
			header("location:frmListarProdutos.php");
		}
		
		
		
		//RIGHT
		public function cadastrarProduto(){
			$objConnection = new Connection();
			
			$data = array(
				'classe' 	=>  mb_strtoupper($this->classe),
				'descricao'	=> mb_strtoupper($this->descricao),
				'valorUnitario'	=> mb_strtoupper($this->valorUnitario)
			);
			$Produtos = $objConnection->DBCreate('Produto', $data);
		}
		
		
		
		public function addCarrinho($idpedido, $descricao){
			$objConnection = new Connection();
			
			$idproduto = $objConnection->DBRead("Produto","WHERE descricao like '%{$descricao}%'", "idProduto");
			
			$data = array(
				'Pedido_idPedido' 	=> $idpedido,
				'Produto_idProduto' 	=> $idproduto[0]['idProduto']
			);
			
			$Produtos = $objConnection->DBCreate('Pedido_has_Produto', $data);
		}
	}
?>
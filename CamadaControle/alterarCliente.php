<?php
if(isset($_POST['salvar']) && $_POST['salvar'] == 'sim'){
	
require_once('../CamadaControle/Classes/Login.php');
require_once('../CamadaControle/Classes/Connection.php');
require_once('../CamadaControle/Classes/Cliente.php');
require_once('../CamadaControle/Classes/Valida.php');

	$novos_campos = array();
	$campos_post = $_POST['campos'];
	$respostas = array();
	
	foreach ($campos_post as $indice => $valor) {
		$novos_campos[$valor['name']] = $valor['value'];
	}
	$objValida = new ValidaCPFCNPJ($novos_campos['cpf']);
	$validado = $objValida->valida();
	if($validado == false){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'CPF/CNPJ Inválido, preencha com CPF/CNPJ Válido';			
	}elseif(!preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $novos_campos['cep'])){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'CEP inválido, Digite um CEP Válido';	
	}elseif(!preg_match('^\(+[0-9]{2,3}\) [0-9]{4}-[0-9]{4}$^', $novos_campos['telefone'])){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'Telefone inválido, Digite um telefone Válido';	
	}elseif(!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $novos_campos['email'])){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'E-mail inválido, Digite um E-mail Válido';	
	}
	else{
		$id 		= $novos_campos['id'];
		$nome 		= $novos_campos["nome"];
		$sobrenome 	= $novos_campos["sobrenome"];
		$email 		= $novos_campos["email"];
		$telefone 	= $novos_campos['telefone'];
		$cpfCnpj 	= $novos_campos["cpf"];
		$rg			= $novos_campos["rg"];
		$endereco 	= $novos_campos["endereco"];
		$bairro 	= $novos_campos["bairro"];
		$cidade 	= $novos_campos["cidade"];
		$estado 	= $novos_campos["estado"];
		$cep		= $novos_campos["cep"];
		$complemento= $novos_campos['complemento'];
		$numero		= $novos_campos['numero'];
		
		$cpfCnpj		= preg_replace( '[^0-9]', '', $cpfCnpj);
		$telefone		= preg_replace( '/[^0-9]/', '', $telefone);
		$cep			= preg_replace( '/[^0-9]/', '', $cep);
		$numero			= preg_replace( '/[^0-9]/', '', $numero);
		$rg				= preg_replace( '/[^0-9]/', '', $rg);		
		
		
		$objCliente = new Cliente();
		$objCliente->setNome($nome);
		$objCliente->setSobrenome($sobrenome);
		$objCliente->setEmail($email);
		$objCliente->setTelefone($telefone);
		$objCliente->setCpfCnpj($cpfCnpj);
		$objCliente->setRg($rg);
		$objCliente->setEndereco($endereco);
		$objCliente->setBairro($bairro);
		$objCliente->setCidade($cidade);
		$objCliente->setEstado($estado);
		$objCliente->setCep($cep);
		$objCliente->setComplemento($complemento);
		$objCliente->setNumero($numero);
		
		$objCliente->alterarCliente($id);
		$respostas['erro'] = 'nao';
		$respostas['msg']  = 'Salvando Pedido...';	
		
	}
	echo json_encode($respostas);	
}	



?>
<?php
require_once('Classes/Login.php');
require_once('Classes/Pedido.php');
require_once('Classes/Connection.php');
require_once('Classes/Cliente.php');
require_once('Classes/Valida.php');
if(isset($_POST['cadastrar']) && $_POST['cadastrar'] == 'sim'){
	$novos_campos = array();
	$campos_post = $_POST['campos'];
	$respostas = array();
	
	foreach ($campos_post as $indice => $valor) {
		$novos_campos[$valor['name']] = $valor['value'];
	}
	$objValida = new ValidaCPFCNPJ($novos_campos['cpf']);
	$validado = $objValida->valida();
	$objCliente = new Cliente();
	
	if($validado == false){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'CPF/CNPJ Inválido, preencha com CPF/CNPJ Válido';			
	}elseif($objCliente->verificarExistencia($novos_campos['cpf']) == false){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'O CPF/CNPJ não pertence a um cliente cadastrado no sistema';	
	}
	elseif($novos_campos['condicao'] > 24 || $novos_campos['condicao'] < 1 ){
		$respostas['erro'] = 'sim';
		$respostas['getErro'] = 'A ´condição de pagamento` deve ser entre 1 e 24 (meses)';	
	}
	else{
		$objPedido = new  Pedido();
		$objPedido->cadastrarPedido($novos_campos);
				
		$respostas['erro'] = 'nao';
		$respostas['msg']  = 'Salvando Pedido...';		
	}
	echo json_encode($respostas);	
}	

if(isset($_POST['pronto'])){
	$id = $_POST['idpedido'];	
	$objPedido = new  Pedido();
	$objPedido->tornarEmAndamento($id);
	header("Location:../CamadaApresentacao/frmListarPedidosAndamento.php");
}
if(isset($_POST['rematar'])){
	$id = $_POST['rematarid'];	
	$objPedido = new  Pedido();
	$objPedido->rematarPedido($id);
	header("Location:../CamadaApresentacao/frmListarPedidosRematados.php");
}
?>

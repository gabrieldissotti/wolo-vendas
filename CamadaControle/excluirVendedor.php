<?php
if(isset($_POST['excluir']) && $_POST['excluir'] == 'sim'){
	
require_once('../CamadaControle/Classes/Login.php');
require_once('../CamadaControle/Classes/Connection.php');
require_once('../CamadaControle/Classes/Vendedor.php');

	$novos_campos = array();
	$campos_post = $_POST['campos'];
	$respostas = array();
	
	foreach ($campos_post as $indice => $valor) {
		$novos_campos[$valor['name']] = $valor['value'];
	}
		$id 		= $novos_campos['id'];
		
		$objVendedor = new Vendedor();
		$objVendedor->setId($id);
		
		$objVendedor->excluirVendedor();
		$respostas['erro'] = 'nao';
		$respostas['msg']  = 'Salvando Pedido...';			
		
		echo json_encode($respostas);	
}	
?>